import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.css';
import App from './container/App';
import registerServiceWorker from './services/registerServiceWorker';

import 'materialize-css/dist/css/materialize.min.css';
import './assets/index.css';

import 'jquery/dist/jquery';
import 'materialize-css/dist/js/materialize';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
