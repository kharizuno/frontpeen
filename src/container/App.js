import React, {Component} from 'react';
import validator from 'validator';

import Tab1 from './tabs/tab1';
import Tab2 from './tabs/tab2';
import Tab3 from './tabs/tab3';

class App extends Component {
    constructor(props) {
        super();

        this.state = {
            tabs: 1,
            data: {
                firstname: '',
                firstnameErr: '',
                lastname: '',
                lastnameErr: '',
                gender: '',
                genderErr: '',
                age: '',
                ageErr: '',
                email: '',
                emailErr: '',
                phone: '',
                phoneErr: '',
                password: '',
                passwordErr: '',
                passwordcf: '',
                passwordcfErr: ''
            }
        };
    }

    updateState(event) {
        const field = event.target.name;
        const state = this.state.data;
        state[field] = event.target.value;
        return this.setState(state);
    }

    onSave(event) {
        const { data } = this.state;
        let tabs = event + 1;
        let msg = '';
        switch (event) {
            case 1:
                msg = (!data.firstname) ? {firstnameErr: 'Firstname is required'} : '';
                msg = (!data.lastname && !msg) ? {lastnameErr: 'Lastname is required'} : msg;
                msg = (!data.gender && !msg) ? {genderErr: 'Gender is required'} : msg;
                msg = (!data.age && !msg) ? {ageErr: 'Age is required'} : msg;
                msg = (!validator.isNumeric(data.age) && !msg) ? {ageErr: 'Age must be number'} : msg;
                break;
            case 2:
                msg = (!data.email) ? {emailErr: 'Email is required'} : '';
                msg = (!validator.isEmail(data.email) && !msg) ? {emailErr: 'Email is not valid'} : msg;
                msg = (!data.phone && !msg) ? {phoneErr: 'Phone is required'} : msg;
                msg = (!validator.isMobilePhone(data.phone, 'any') && !msg) ? {phoneErr: 'Phone is not valid'} : msg;
                break;
            case 3:
                msg = (!data.password) ? {passwordErr: 'Password is required'} : '';
                msg = (!data.passwordcf && !msg) ? {passwordcfErr: 'Confirm Password is required'} : msg;
                msg = (data.password !== data.passwordcf && !msg) ? {passwordcfErr: 'Confirm Password is not match'} : msg;
                break;

            default:
        }

        if (!msg) {
            this.setState({tabs: tabs});
        } else {
            let newdata = {...data, ...msg};
            this.setState({data: newdata});
        }
    }

    render() {
        const { tabs } = this.state;
        let finish = (tabs === 4) ? 'col s12 mg-t15 disp-block' : 'col s12 mg-t15 disp-none';

        return (
            <div className="App container-fluid">
                <div className="row">
                    <div className="col s12">
                        <ul className="tabs-form">
                            <li className="tab col s3"><a className={(tabs === 1) ? 'active' : ''} href="#person"><i className="material-icons">person</i></a></li>
                            <li className="tab col s3"><a className={(tabs === 2) ? 'active' : ''} href="#email"><i className="material-icons">email</i></a></li>
                            <li className="tab col s3"><a className={(tabs === 3) ? 'active' : ''} href="#password"><i className="material-icons">settings</i></a></li>
                            <li className="tab col s3"><a className={(tabs === 4) ? 'active' : ''} href="#success"><i className="material-icons">thumb_up</i></a></li>
                        </ul>
                    </div>
                    <div className="form">
                        <Tab1 {...this.state.data} tabs={tabs} changes={(e) => this.updateState(e)} saved={(e) => this.onSave(e)} />
                        <Tab2 {...this.state.data} tabs={tabs} changes={(e) => this.updateState(e)} saved={(e) => this.onSave(e)} />
                        <Tab3 {...this.state.data} tabs={tabs} changes={(e) => this.updateState(e)} saved={(e) => this.onSave(e)} />
                        <div id="success" className={finish}>
                            <div className="ff-16 tx-center cl-grey">
                                <div className="ff-20 mg-b15 cl-green">
                                    <i className="material-icons">thumb_up</i>
                                </div>
                                Register Successfully
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
