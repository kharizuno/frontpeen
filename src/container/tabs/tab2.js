import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TextInput from '../Textinput';

class Tab2 extends Component {
    constructor(props) {
        super();
    }

    render() {
        const { tabs } = this.props;
        let open = (tabs === 2) ? 'col s12 mg-t15 disp-block' : 'col s12 mg-t15 disp-none';

        return (
            <div id="email" className={open}>
                <TextInput
                    name="email"
                    type="email"
                    value={this.props.email}
                    error={this.props.emailErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Email"/>

                <TextInput
                    name="phone"
                    value={this.props.phone}
                    error={this.props.phoneErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Phone"/>

                <input
                    type="submit"
                    value="Next"
                    className="btn btn-primary wd-100"
                    onClick={(e) => this.props.saved(2)}/>
            </div>
        );
    }
};

Tab2.propTypes = {
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired
};

export default Tab2;
