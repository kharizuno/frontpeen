import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TextInput from '../Textinput';

class Tab3 extends Component {
    constructor(props) {
        super();
    }

    render() {
        const { tabs } = this.props;
        let open = (tabs === 3) ? 'col s12 mg-t15 disp-block' : 'col s12 mg-t15 disp-none';

        return (
            <div id="password" className={open}>
                <TextInput
                    name="password"
                    type="password"
                    value={this.props.password}
                    error={this.props.passwordErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Password"/>

                <TextInput
                    name="passwordcf"
                    type="password"
                    value={this.props.passwordcf}
                    error={this.props.passwordcfErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Confirm Password"/>

                <input
                    type="submit"
                    value="Finish"
                    className="btn btn-primary wd-100"
                    onClick={(e) => this.props.saved(3)}/>
            </div>
        );
    }
};

Tab3.propTypes = {
    password: PropTypes.string.isRequired,
    passwordcf: PropTypes.string.isRequired
};

export default Tab3;
