import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TextInput from '../Textinput';

class Tab1 extends Component {
    constructor(props) {
        super();
    }

    render() {
        const { tabs } = this.props;
        let open = (tabs === 1) ? 'col s12 mg-t15 disp-block' : 'col s12 mg-t15 disp-none';

        return (
            <div id="person" className={open}>
                <TextInput
                    name="firstname"
                    value={this.props.firstname}
                    error={this.props.firstnameErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Firstname"/>

                <TextInput
                    name="lastname"
                    value={this.props.lastname}
                    error={this.props.lastnameErr}
                    onChange={(e) => this.props.changes(e)}
                    placeholder="Lastname"/>

                <div className="row-reset">
                    <div className="col s8">
                        <div className="field">
                            <select name="gender" className="disp-block" defaultValue="" onChange={(e) => this.props.changes(e)}>
                                <option value="" disabled>Gender</option>
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                            {this.props.genderErr && <div className="alert alert-danger">{this.props.genderErr}</div>}
                        </div>
                        {/*<TextInput
                            name="gender"
                            value={this.props.gender}
                            error={this.props.genderErr}
                            onChange={(e) => this.props.changes(e)}
                            placeholder="Gender"/>*/}
                    </div>
                    <div className="col s4">
                        <TextInput
                            name="age"
                            type="number"
                            value={this.props.age}
                            error={this.props.ageErr}
                            onChange={(e) => this.props.changes(e)}
                            options={{min: 0}}
                            placeholder="Age"/>
                    </div>
                </div>

                <input
                    type="submit"
                    value="Next"
                    className="btn btn-primary wd-100"
                    onClick={(e) => this.props.saved(1)}/>
            </div>
        );
    }
};

Tab1.propTypes = {
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    age: PropTypes.string.isRequired
};

export default Tab1;
